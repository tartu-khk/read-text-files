/*     
    1. (kergem) Leia tekstist enimkasutatud täht, ning tagasta mitu korda seda tähte artiklis esineb.
    2. (raskem) Leia tekstist pikim sõna(d) milles ei kordu ükski täht.  
*/


function readSingleFile(file) {

    const selectedFile = file.target.files[0];

    if (selectedFile) {
        let r = new FileReader();
        r.onload = function(e) {
            let contents = e.target.result;
            let letter = findMostUsedLetter(contents);
            let words = findLongestWord(contents);

            writeResults(letter[0], letter[1], words);
        };
        r.readAsText(selectedFile);
    } else { 
        alert('Failed to load file');
    }

}


function findMostUsedLetter(contents) {

    let toArray = contents.toLowerCase().replace(/[-+().,!?:;='´"_0-9\s]/g, '').split('');
    let countLetters = [];
    let mostUsedLetter = '';
    let timesUsed = 0;

    toArray.forEach(function (v, i) {
        if (!countLetters[v]) {
            countLetters[v] = [i];
        } else {
            countLetters[v].push(i);
        }
    });

    for (const key in countLetters) {
        const v = countLetters[key].length;
        if (timesUsed < v) {
            mostUsedLetter = key;
            timesUsed = v;
        }
    }

    return [mostUsedLetter, timesUsed]

}


function findLongestWord(contents) {

    let toWordArray = contents.toLowerCase().replace(/[-+().,!?:;='´"_0-9\s]/g, ' ').split(' ');
    let uniqueArray = [];

    toWordArray.forEach(function(word){
        let duplicate = 0;
        for(let i = 0; i < word.length; i++) {
           let letter = word.charAt(i);
           let check = word.indexOf(letter, i+1);
           if (check > 0) {
               duplicate++
           }
        }
        if (duplicate === 0){
            uniqueArray.push(word);
        }
    });

    uniqueArray.sort(function(a,b){
        return b.length - a.length;
    });

    return uniqueArray.filter(function(word) {
        if (word.length === uniqueArray[0].length){
            return word;
        }
    });

}


function writeResults(mostUsedLetter, mostUsedLetterCount, longestWord) {

    document.getElementById('file-output').innerHTML +=
        'Enimkasutatud täht: ' + mostUsedLetter + '<br>' +
        'Esinemise sagedus: ' + mostUsedLetterCount + ' korda<br>' +
        'Pikim sõna(d): ' + longestWord + '<br>';

}


document.getElementById('file-input').addEventListener('change', readSingleFile);